var enabled = false;
var prev = '';
chrome.storage.sync.set({active: false}, function() {
	console.log('Active: ' + enabled);
});
function attempt_post(song, url) {
	if(song === prev) {return;}

	prev = song;
	params = 'song=' + song.replace('&', '%26') + '&url=' + encodeURI(url);

	let xhr = new XMLHttpRequest();
	xhr.open('POST', 'http://127.0.0.1:5000?' + params);
	xhr.send();
}

function find_song()  {
	let valid_site = (url) => {
		let domains = ['youtube.com', 'soundcloud.com'];
		for(i in domains) {
			if(url.includes(domains[i])) {
				return true;
			}
		}
		return false;
	}
	// if it wasn't for youtube jamming its name in the title this wouldn't be a thing
	let clean_title = (title) => {
		let f = '- YouTube';
		if(title.includes(f)) {
			return title.replace(f, ' ');
		}
		return title;
	}
	// Iteration to find the tab we need 
	chrome.windows.getAll({populate:true}, function(windows){
		windows.forEach(function(window) {
			window.tabs.forEach(function(tab){
				if(valid_site(tab.url) && tab.audible) {
					chrome.storage.sync.get('active', function(result) {
						if(result['active']) {
							attempt_post(clean_title(tab.title), tab.url);
						}
					});
				}
			})
		})
	})
}
chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		if(request.active == true)  {
			chrome.storage.sync.set({active: true}, function() {
				enabled = true;
			});
			sendResponse({response: 'enabled'});
		}
		else {
			chrome.storage.sync.set({active: false}, function() {
				enabled = false;
			});
			sendResponse({response: 'disabled'});
		}
	}
);
setInterval(find_song, 5000);
