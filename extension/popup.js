// just get the value setup by the background script first
var image = document.getElementById('image');
chrome.storage.sync.get('active', function(result) {
	if(result['active']) {
		image.src = '/images/disable.png';
	}
});

// Give the thing some clickableness
image.addEventListener('click', function() {
	console.log('src is :' + image.src);
	if(image.src.includes('enable')) {
		image.src = '/images/disable.png';
		chrome.runtime.sendMessage({active: true}, function(response) {
			console.log(response);
		});
	}
	else if(image.src.includes('disable')) {
		image.src = '/images/enable.png';
		chrome.runtime.sendMessage({active: false}, function(response) {
			console.log(response);
		});
	}
});
