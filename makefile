win=x86_64-pc-windows-gnu
cargo=cargo build

linux:
	$(cargo)

linux-release:
	$(cargo) --release


# None of these work windows binaries will come from a seperate server instead
# This means win binary deployment will be really fucking slow but fuck this anyway
windows:
	#rustup run nightly cargo rustc --target=x86_64-pc-windows-gnu -- -C linker=x86_64-w64-mingw32-gcc
	$(cargo) --target $(win)

windows-release:
	$(cargo) --release --target $(win)


run:
	# really only testing linux builds because well linux
	cargo run
