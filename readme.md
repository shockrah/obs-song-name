# OBS Song Name Chrome Extension

> Supported sites?

Youtube, Soundcloud.

> Can you add more platforms pls

Find me on discord/email/open an issue/smoke signal whatever: contact info is at the bottom.


> When will it be done?

Once the following is done.


1. Upload extension onto chrome store.
2. Setup CI/CD for windows binaries


> Can I help?

Sure if there's anything in the todo list that you want to do just message me on discord or make a merge request here.
Bug reports are also welcome on the issues page :).

# Contact 

Discord: `shockrah#2647`	<-- preferred
Email: `mail@shockrah.shop`
