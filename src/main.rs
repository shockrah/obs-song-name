#[macro_use] extern crate nickel;
use nickel::{Nickel, HttpRouter, QueryString};
use std::fs;

fn main() {
    let mut server = Nickel::new();
    server.post("/", middleware!{ |request|
        let mut params = String::new();
        if let Some(song) = request.query().get("song") {
                println!("Song: {}", song);
                fs::write("song_name.txt", song).expect("unable to write");
                params.push_str(song);
        }

        if let Some(url) = request.query().get("url") {
            println!("URL: {:?}", url);
        }

        format!("{}     ", params.as_str())
    });
    println!("Find me on gitlab or twitch ;)");
    println!("  gitlab.com/shockrah");
    println!("  twitch.tv/shockrah");
    server.listen("127.0.0.1:5000").unwrap();
}
